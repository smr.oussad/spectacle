package projetPOO.main;

import java.util.InputMismatchException;
import java.util.Scanner;

import projetPOO.Horaire;
import projetPOO.gestionSpectacle.GestionProgrammationSemaine;

public class Main {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        GestionProgrammationSemaine gestion = new GestionProgrammationSemaine();
        System.out.println("bienvenu sur notre plateforme de gestion de programmation de semaine");
        do {
            System.out.println("Entrez : \n"
                    + "\"0\" pour reinitialiser le programme\n"
                    +"\"1\" pour ajouter un film \n"
                    +"\"2\" ajouter une pièce de théâtre\n"
                    +"\"3\" ajouter un nom d'interprète à un spectacle \n"
                    +"\"4\" ajouter une séance pour un film\n"
                    +"\"5\" ajouter une séance pour une pièce de théâtre\n"
                    +"\"6\" vendre des places pour un film \n"
                    +"\"7\" vendre des places pour une pièce de théâtre\n"
                    +"\"8\" consulter le chiffre d'affaires et le taux de remplissage d'un spectacle\n"
                    +"\"9\" pour quitter\n"
            );

            int methodes = sc.nextInt();
            
            try{
                if (methodes == 0) {
                    gestion.reinitialiserProgrammation();
                    System.out.println("programme réalisé avec succès");
                }
                else if (methodes == 1) {
                    System.out.println("donne un titre, realisateur et une duree");
                    String titre = sc.next();
                    String realisateur = sc.next();
                    int duree = sc.nextInt();
                    gestion.ajouterFilm(titre, realisateur, duree);
                }
                else if (methodes == 2) {
                    System.out.println("donne un titre, metteurenscene et un nbr entractes");
                    String titre = sc.next();
                    String nom = sc.next();
                    int entractes = sc.nextInt();
                    gestion.ajouterPiece(titre, nom, entractes);
                }
                else if (methodes==3){
                    System.out.println(gestion.lesFilms());
                    System.out.println(gestion.lesPieces());
                    System.out.println("donne une nom interpretre et un numero de spectacle");
                    String nom = sc.next();
                    int num = sc.nextInt();
                    gestion.ajouterInterprete(num,nom);
                }
                else if (methodes==4){
                    System.out.println(gestion.lesFilms());
                    System.out.println("donne un id de film,jour,heure,minute et un idsalle");
                    int id = sc.nextInt();
                    int jour = sc.nextInt();
                    int heure = sc.nextInt();
                    int minute = sc.nextInt();
                    int idsalle = sc.nextInt();
                    gestion.ajouterSeanceFilm(id,jour,new Horaire(heure,minute),idsalle);
                }
                else if (methodes==5){
                    System.out.println(gestion.lesPieces());
                    System.out.println("donne un id de theatre,jour,heure,minute et un idsalle");
                    int id = sc.nextInt();
                    int jour = sc.nextInt();
                    int heure = sc.nextInt();
                    int minute = sc.nextInt();
                    int idsalle = sc.nextInt();
                    gestion.ajouterSeanceTheatre(id,jour,new Horaire(heure,minute),idsalle);
                }
                else if(methodes==6){
                    boolean check = false;
                    int id = 0, jour = 0, heure = 0, minute = 0;
                    do{
                        try{
                            System.out.println(gestion.lesFilms());
                            System.out.println("donne un id");
                            id = sc.nextInt();
                            System.out.println(gestion.lesSeancesFilm(id));
                            System.out.println("jour, heure et minute");
                            jour = sc.nextInt();
                            heure = sc.nextInt();
                            minute = sc.nextInt();
                            System.out.println(gestion.getNbPlacesDispo(id, jour, heure, minute));
                            check = true;
                        }catch (IllegalArgumentException e){
                            System.out.println(e.getMessage());
                            continue;
                        }
                    }while(!check);
                    System.out.println("puis nb place tarif normal et tarif reduit");
                    int nbTN = sc.nextInt();
                    int nbTR = sc.nextInt();
                    gestion.vendrePlaceFilmTN(id,jour,new Horaire(heure,minute),nbTN);
                    gestion.vendrePlaceFilmTR(id,jour,new Horaire(heure,minute),nbTR);

                }
                else if (methodes==7){
                    boolean check = false;
                    int id = 0, jour = 0;
                    do{
                        try{
                            System.out.println(gestion.lesPieces());
                            System.out.println("donne un id");
                            id = sc.nextInt();
                            System.out.println(gestion.lesSeancesTheatre(id));
                            System.out.println("donne un jour");
                            jour = sc.nextInt();
                            System.out.println(gestion.getNbPlacesDispo(id,jour));
                            System.out.println(gestion.getNbFauteuilsDispo(id,jour));
                            check = true;
                        }catch (IllegalArgumentException e ){
                            System.out.println(e.getMessage());
                            continue;
                        }
                    }while(!check);
                    System.out.println("donne un nombre de place standard et nb de fauteuils");
                    int nbTN = sc.nextInt();
                    int nbF = sc.nextInt();
                    gestion.vendrePlacePieceTN(id,jour,nbTN);
                    gestion.vendrePlaceFauteuilPiece(id,jour,nbF);

                }
                else if (methodes==8){
                    System.out.println(gestion.lesFilms());
                    System.out.println(gestion.lesPieces());
                    System.out.println("donne num du spectacle");
                    int num = sc.nextInt();
                    System.out.println("chiffre affaire : "+gestion.chiffreAffaires(num));
                    System.out.println("taux remplissage : "+gestion.getTauxRemplissage(num));
                }
                else if (methodes==9){
                    break;
                }
                else {
                    throw new IllegalArgumentException("methodes inexistante");
                }
            }catch (IllegalArgumentException | IllegalStateException e){
                System.out.println(e.getMessage());
            }
            catch (InputMismatchException e){
                System.out.println("saisie invalide");
                sc.next();
            }
            
        }while(true);
    }
}
