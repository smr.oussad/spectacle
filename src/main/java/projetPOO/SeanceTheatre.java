package projetPOO;

public class SeanceTheatre extends Seance {

    private SalleTheatre salleDeTheatre;

    private int nbFauteuilsVendues;

    public SeanceTheatre(Creneau creneau, SalleTheatre salleDeTheatre) {
        super(creneau, salleDeTheatre);
        this.salleDeTheatre=salleDeTheatre;
    }

    public int getNbFauteuilsVendues(){
        return nbFauteuilsVendues;
    }

    public void setNbFauteuilsVendues(int nbre) {
        this.nbFauteuilsVendues+= nbre;
    }

    public double getTarifFauteuils () {
        return salleDeTheatre.getTarifsFauteuils();
    }

    public double getTarifNormal () {
        return salleDeTheatre.getTarifplace();
    }

    public int getNbFauteuils(){
        return salleDeTheatre.getNbFauteuils();
    }

    public int getNbFauteuilsDispo() {
        return getNbFauteuils() - getNbFauteuilsVendues();
    }

    /**
     * @param nbre de fauteuils
     * @throws IllegalArgumentException si la limite du nombre de fauteuils dans la salle est atteinte
     */
    public void vendrePlacesFauteuil(int nbre){
        if(getNbFauteuilsVendues() + nbre <= getNbFauteuils()){
            setNbFauteuilsVendues(nbre);
        }
        else{
            throw new IllegalArgumentException ("Limite de place vendue atteinte");
        }
    }

    @Override
    public void vendrePlacesTN(final int nbre) {
        if (getnbplaceVendueTarifNormal() + nbre <= getnbplace()){
            setNbplaceVendueTarifNormal(nbre);
        } else {
            throw new IllegalArgumentException ("Limite de place vendue atteinte");
        }
    }

    @Override
    public double tauxRemplissage() {
        return ((double)(getnbplaceVendueTarifNormal() + getNbFauteuilsVendues()) /
                (double)(getnbplace() + getNbFauteuils())) * 100;
    }

    @Override
    public int getNbPlaceDispo() {
        return getnbplace() - getnbplaceVendueTarifNormal();
    }

    public SalleTheatre getSalleDeTheatre() {
        return salleDeTheatre;
    }

    @Override
    public String toString() {
        return super.toString()+"\n Salle : " +getSalleDeTheatre().toString()+"\n nb Fauteils vendu : "+ nbFauteuilsVendues;
    }
}
