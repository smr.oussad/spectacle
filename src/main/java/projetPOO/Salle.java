package projetPOO;

import java.util.*;

public class Salle {

    private int numSalle;

    private int nbplace;

    private String nomSalle;

    private double tarifplace;

    private Map<Integer,Set<Creneau>> lesCreneauxOccupes;

    private static int numSalleSuivant = 10;

    public Salle(int nbplace, String nomSalle, double tarifplace) {
        this.numSalle = numSalleSuivant;
        numSalleSuivant+=10;
        this.nbplace = nbplace;
        this.nomSalle = nomSalle;
        this.tarifplace = tarifplace;
        lesCreneauxOccupes = new TreeMap<Integer, Set<Creneau>>();
    }

    /**
     * @param creneau
     * @return true ou false si le créneau passé en parametre est disponible ou non
     */
    public boolean estDisponible(Creneau creneau){
        //On verifie qu'il y a des créneaux à ce jour
        if(!pasDeCreneauCeJour(creneau.getJours())) {
            for(Creneau creneauOccupe : lesCreneauxOccupes.get(creneau.getJours())) {
                if(!creneauOccupe.estDispo(creneau.getJours(), creneau.getDebut(), creneau.getFin())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *
     * @param creneau
     * @return Vrai quand on reussi a ajouter le créneau, sinon faux
     */
    public boolean ajouterCreneau (Creneau creneau){
        if(!estDisponible(creneau)){
            return false;
        } else {
            if(pasDeCreneauCeJour(creneau.getJours())) {
                Set<Creneau> newCreneau = new HashSet<>() ;
                newCreneau.add(creneau);
                lesCreneauxOccupes.put(creneau.getJours(), newCreneau);
            } else {
                lesCreneauxOccupes.get(creneau.getJours()).add(creneau);
            }
            return true;
        }
    }

    /**
     *
     * @param jour doit etre entre 1 et 7
     * @return vrai s'il n'y a pas de creneau au jour passé en parametre, faux sinon
     * @throws IllegalArgumentException
     */
    public boolean pasDeCreneauCeJour (int jour){
        if (jour <1 || jour >7){
            throw new IllegalArgumentException("Jours invalide");
        }
        return !lesCreneauxOccupes.containsKey(jour);
    }

    public void supprimerCreneauOccupees(){
        lesCreneauxOccupes.clear();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Salle)) return false;
        final Salle salle = (Salle) o;
        return numSalle == salle.numSalle &&
                nbplace == salle.nbplace &&
                Double.compare(salle.tarifplace, tarifplace) == 0 &&
                nomSalle.equals(salle.nomSalle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numSalle, nbplace, nomSalle);
    }

    public Map<Integer, Set<Creneau>> getLesCreneauxOccupes() {
        return lesCreneauxOccupes;
    }

    public int getNumSalle() {
        return numSalle;
    }

    public String getNomSalle() {
        return nomSalle;
    }

    public int getNbplace() {
        return nbplace;
    }

    public double getTarifplace(){
        return tarifplace;
    }

    public double getTarifplaceReduit(){
        return tarifplace * 0.6;
    }

    public void setNbplace(int nbplace) {
        this.nbplace = nbplace;
    }

    @Override
    public String toString(){
      return ("Salle n°"+numSalle+"\nNom Salle : "+nomSalle+"\nNb Place :"+nbplace+"\nTarif Place : "+tarifplace) ;
    }

}
