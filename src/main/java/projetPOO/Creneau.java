package projetPOO;

import java.util.Objects;

public class Creneau {
    private int jours;
    private Horaire debut;
    private Horaire fin;

    /**
     * @param jours entre 1 et 7
     * @throws IllegalArgumentException lorsque le jour n'est pas compris entre 1 et 7
     */
    public Creneau(int jours, Horaire debut, Horaire fin) {
        if(jours>7 || jours <1){
            throw new IllegalArgumentException("Les jours sont entre 1 et 7");
        }
        this.jours=jours;
        this.debut = debut;
        this.fin=fin;
    }

    /**
     * verification de la disponibilité du creneau.
     * @param jours
     * @param debut
     * @param fin
     * @return true si dispo sinon false.
     */
    public boolean estDispo(int jours, Horaire debut, Horaire fin) {
        //Contrôle sur les horaires de début et de fin ? ex: estDispo(1, H(10,0), H(9,0))
        return (jours!=this.jours || this.debut.compareTo(fin) <= 0 || this.fin.compareTo(debut) >= 0);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Creneau creneau = (Creneau) object;
        return jours == creneau.jours &&
                Objects.equals(debut, creneau.debut) &&
                Objects.equals(fin, creneau.fin);

    }

    @Override
    public int hashCode() {
        return Objects.hash(jours, debut, fin);
    }

    public int getJours() {
        return jours;
    }

    public Horaire getDebut() {
        return debut;
    }

    public Horaire getFin() {
        return fin;
    }

    @Override
    public String toString() {
        return "Le jours :"+ jours +" de " + debut + " à "+ fin;
    }
}
