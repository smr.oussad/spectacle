package projetPOO;

import java.util.Objects;

public abstract class Seance {

    protected Creneau creneau;

    protected Salle salle;

    protected int nbplaceVendueTarifNormal;

    protected Seance(Creneau creneau, Salle salle){
        this.creneau=creneau;
        this.salle=salle;
    }

    /**
     * @param nbre de place vendueTN ne doit pas dépasser le nombre de place disponible dans la salle
     * @throws IllegalArgumentException si la limite de places vendues est atteinte
     */
    public abstract void vendrePlacesTN(int nbre);

    /**
     * @return le taux de remplissage de la salle.
     */
    public abstract double tauxRemplissage();

    public int getnbplace(){
        return salle.getNbplace();
    }

    public abstract int getNbPlaceDispo();

    public int getnbplaceVendueTarifNormal(){
        return nbplaceVendueTarifNormal;
    }

    public Horaire getdebut(){
        return creneau.getDebut();
    }

    public Horaire getfin(){
        return creneau.getFin();
    }

    public int getjours(){
        return creneau.getJours();
    }

    public double getTarif (){
        return salle.getTarifplace();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Seance seance = (Seance) object;
        return creneau.equals(seance.creneau);
    }

    @Override
    public int hashCode() {
        return Objects.hash(salle, creneau);
    }

    protected void setNbplaceVendueTarifNormal(int nbre){
        nbplaceVendueTarifNormal+=nbre;
    }

    public Salle getSalle() {
        return salle;
    }

    @Override
    public String toString() {
        return "Creneau : "+creneau.toString()+"\n nb places vendues tarif normal : " + nbplaceVendueTarifNormal;
    }
}
