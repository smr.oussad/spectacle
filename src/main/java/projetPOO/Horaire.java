package projetPOO;


import java.util.Objects;

public class Horaire implements Comparable<Horaire>{

    private int minutes;

    /**
     * @param "heures comprises entre 0 et 23"
     * @param "minutes comprises entre 0 et 59"
     */
    public Horaire(int heures, int minutes) {

        if((heures>23 || heures <0)&&(minutes>59 || minutes<0)){
            throw new IllegalArgumentException("heures invalides");
        }

        this.minutes=heures*60+minutes;

    }

    public int getHeures(){
        return minutes/60;
    }

    public int getMinutes(){
        return minutes%60;
    }

    @Override
    public int compareTo(Horaire horaire) {
        return Integer.compare(horaire.minutes, this.minutes);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;
        Horaire horaire = (Horaire) object;
        return minutes == horaire.minutes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(minutes);
    }

    @Override
    public String toString() {
        return getHeures()+"h"+getMinutes()+"minutes. ";
    }

}
