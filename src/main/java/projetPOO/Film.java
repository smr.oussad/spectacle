package projetPOO;

import java.util.ArrayList;
import java.util.Collection;

public class Film extends Spectacle {

    private static int numeroFilm = 100;

    private String nomRealisateur;

    private int duree;

    public Film(String titre, String nomRealisateur, int duree) {
        super(numeroFilm++, titre);
        this.nomRealisateur = nomRealisateur;
        this.duree = duree;
    }

    /**
     * création d'une liste puis parcours la hashset de seances et récupère tous les film qui ont pour jour celui donné en paramètre.
     * @param jour entre 1 et 7.
     * @return la liste.
     */
    @Override
    public Collection<Seance> rechercherSeances(int jour) {
        Collection<Seance> rechercherSeances = new ArrayList<>();
        for (Seance seance : seances){
            if (seance.getjours()==jour){
                rechercherSeances.add(seance);
            }
        }
        return rechercherSeances;
    }

    /**
     * parcours de la hashset seances et calcul le chiffreAffaire en fonction des places vendues tarifs normal et réduit.
     * @return le resultat.
     */
    @Override
    public double chiffreAffaire() {
        double result = 0;
        for (Seance seance : seances){
            SeanceFilm seanceFilm = (SeanceFilm) seance;
            result+=seanceFilm.getNbPlacesVenduesTarifReduit() * seanceFilm.getTarifReduit() +
                    seance.getnbplaceVendueTarifNormal() * seance.getTarif();
        }
        return result;
    }

    /**
     * ajout de la seanceFilm à la hashset si elle existe pas.
     * @param seanceFilm
     * @return true si seance est ajoutée sinon false et ne l'ajoute pas.
     */
    public boolean ajouterSeanceFilm (SeanceFilm seanceFilm){
        return seances.add(seanceFilm);
    }

    public String getNomRealisateur() {
        return nomRealisateur;
    }

    public int getDuree() {
        return duree;
    }

    @Override
    public String toString() {
        return super.toString()+"\n"+"nom du réalisateur :"+nomRealisateur+"\n"+"durée : "+duree;

    }
}
