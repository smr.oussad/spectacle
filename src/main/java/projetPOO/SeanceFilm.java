package projetPOO;

public class SeanceFilm extends Seance {

    private int nbPlacesVenduesTarifReduit;

    public SeanceFilm(Creneau creneau, Salle salle) {
        super(creneau, salle);
    }

    @Override
    public void vendrePlacesTN(final int nbre) {
        if (getnbplaceVendueTarifNormal() + getNbPlacesVenduesTarifReduit() + nbre <= getnbplace()){
            setNbplaceVendueTarifNormal(nbre);
        } else {
            throw new IllegalArgumentException ("Limite de place vendue atteinte");
        }
    }

    @Override
    public double tauxRemplissage() {
        return ((double)(getnbplaceVendueTarifNormal() + getNbPlacesVenduesTarifReduit()) / (double)getnbplace()) * 100;
    }

    @Override
    public int getNbPlaceDispo() {
        return getnbplace() - (getnbplaceVendueTarifNormal() + getNbPlacesVenduesTarifReduit());
    }

    /**
     * @param nbre de places vendues au tarif réduit
     * @throws IllegalArgumentException si la limite de places dispo dans la salle est atteinte
     */
    public void vendrePlacesTR(int nbre){
        if(getNbPlacesVenduesTarifReduit() + getnbplaceVendueTarifNormal() + nbre <= getnbplace()){
            setNbPlacesVenduesTarifReduit(nbre);
        }
        else{
            throw new IllegalArgumentException ("Limite de place vendue atteinte");
        }
    }
    @Override
    public String toString() {
        return super.toString()+"\n Salle :"+getSalle().toString()+"\n nb places vendues TR : "+nbPlacesVenduesTarifReduit;
    }

    public int getNbPlacesVenduesTarifReduit(){
        return nbPlacesVenduesTarifReduit;
    }

    public void setNbPlacesVenduesTarifReduit(int nbre) {
        this.nbPlacesVenduesTarifReduit+=nbre;
    }

    public double getTarifReduit(){
        return salle.getTarifplaceReduit();
    }
}

