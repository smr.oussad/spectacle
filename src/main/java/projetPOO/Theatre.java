package projetPOO;

import java.util.ArrayList;
import java.util.Collection;

public class Theatre extends Spectacle {

    private static int numeroTheatre = 1000;

    private String nomMetteurEnScene;

    private int nbEntractes;

    public Theatre(String titre, String nomMetteurEnScene, int nbEntractes) {
        super(numeroTheatre++, titre);
        this.nomMetteurEnScene = nomMetteurEnScene;
        this.nbEntractes = nbEntractes;
    }

    /**
     * création d'une liste et parcours de la hashset seance et si le jour est égale au jour en paramètre ajoute la seance dans la liste et s'arrete
     * car 1 seule piece par jour.
     * @param jour entre 1 et 7.
     * @return la liste.
     */
    @Override
    public Collection<Seance> rechercherSeances(int jour) {
        Collection<Seance> rechercherSeances = new ArrayList<>();
        for (Seance seance : seances){
            if (seance.getjours()==jour){
                rechercherSeances.add(seance);
                break;
            }
        }
        return rechercherSeances;
    }
    /**
     * parcours de la hashset seances et calcul le chiffreAffaire en fonction des places vendues tarifs normal et fauteuils.
     * @return le resultat.
     */
    @Override
    public double chiffreAffaire() {
        double result = 0;
        for (Seance seance : seances){
            SeanceTheatre seanceTheatre = (SeanceTheatre)seance;
            result += seanceTheatre.getNbFauteuilsVendues() * seanceTheatre.getTarifFauteuils() +
                    seanceTheatre.getnbplaceVendueTarifNormal() * seanceTheatre.getTarifNormal();
        }
        return result;
    }
    /**
     * ajout de la seanceTheatre à la hashset si elle existe pas le même jour.
     * @param seanceTheatre
     * @return true si seance est ajoutée sinon false et ne l'ajoute pas.
     */
    public boolean ajouterSeanceTheatre (SeanceTheatre seanceTheatre){
        if (rechercherSeances(seanceTheatre.getjours()).isEmpty()){
            return seances.add(seanceTheatre);
        }
        return false;
    }

    public String getNomMetteurEnScene() {
        return nomMetteurEnScene;
    }

    public int getNbEntractes() {
        return nbEntractes;
    }

    @Override
    public String toString() {
        return super.toString()+"\n"+"nom du metteur en scène : "+nomMetteurEnScene+"\n"+"nombre d'entract : "+nbEntractes;
    }
}
