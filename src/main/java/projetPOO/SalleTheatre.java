package projetPOO;

public class SalleTheatre extends Salle {

    private int nbFauteuils;

    private double tarifsFauteuils;

    public SalleTheatre(int nbplace, String nomSalle, double tarifplace,int nbFauteuils, double tarifsFauteuils) {
        super(nbplace, nomSalle, tarifplace);
        this.nbFauteuils=nbFauteuils;
        this.tarifsFauteuils=tarifsFauteuils;
    }

    public int getNbFauteuils(){
        return nbFauteuils;
    }

    public double getTarifsFauteuils(){
        return tarifsFauteuils;
    }

    public void setNbFauteuils(int nbFauteuils) {
        this.nbFauteuils = nbFauteuils;
    }

    @Override
    public String toString() {
        return super.toString()+"\n nombre de fauteuils :"+nbFauteuils+"\n tarif fauteuils:"+tarifsFauteuils;
    }
}
