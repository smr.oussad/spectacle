package projetPOO.gestionSpectacle;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import projetPOO.*;

public class GestionProgrammationSemaine implements IProgrammationSemaine {

    private Map<Integer, Salle> lesSalles;
    private Map<Integer, SalleTheatre> lesSallesTheatre;
    private Map<Integer, Film> lesFilms;
    private Map<Integer, Theatre> lesPieces;

    public GestionProgrammationSemaine (){
        this.lesSalles= new HashMap<>();
        this.lesSallesTheatre= new HashMap<>();
        this.lesFilms = new HashMap<>();
        this.lesPieces = new HashMap<>();

        Salle salle1 = new Salle(45, "salle1", 12);
        Salle salle2 = new Salle(45, "salle2", 12);
        Salle salle3 = new Salle(45, "salle3", 12);
        Salle salle4 = new Salle(45, "salle4", 12);

        SalleTheatre salleTheatre1 = new SalleTheatre(45,"salletheatre1", 12,25,30);
        SalleTheatre salletheatre2 = new SalleTheatre(45,"salletheatre2", 12,25,30);
        SalleTheatre salleTheatre3 = new SalleTheatre(45,"salletheatre3", 12,25,30);
        SalleTheatre salleTheatre4= new SalleTheatre(45,"salletheatre4", 12,25,30);

        lesSalles.put(salle1.getNumSalle(),salle1);
        lesSalles.put(salle2.getNumSalle(),salle2);
        lesSalles.put(salle3.getNumSalle(),salle3);
        lesSalles.put(salle4.getNumSalle(),salle4);

        lesSallesTheatre.put(salleTheatre1.getNumSalle(), salleTheatre1);
        lesSallesTheatre.put(salletheatre2.getNumSalle(), salletheatre2);
        lesSallesTheatre.put(salleTheatre3.getNumSalle(), salleTheatre3);
        lesSallesTheatre.put(salleTheatre4.getNumSalle(), salleTheatre4);
    }

    //samir
    @Override
    public Film rechercherFilm(String titre, String realisateur) {
        for (Film film : lesFilms.values()){
            if (film.getTitre().equals(titre) && film.getNomRealisateur().equals(realisateur)){
                return film;
            }
        }
        return null;
    }

    //samir
    @Override
    public void ajouterFilm(String titre, String realisateur, int duree) {
        if (rechercherFilm(titre, realisateur) == null){
            Film film = new Film(titre, realisateur, duree);
            lesFilms.put(film.getNumero(), film);
        }
        else {
            throw new IllegalArgumentException("le film existe déjà");
        }

    }

    //samir
    @Override
    public void ajouterInterprete(int numSpectacle, String interprete) {
        if (estUnFilm(numSpectacle)){
            lesFilms.get(numSpectacle).getInterpretes().add(interprete);
        }
        else if (estUnePiece(numSpectacle)){
            lesPieces.get(numSpectacle).getInterpretes().add(interprete);
        }
        else {
            throw new IllegalArgumentException("Spectacle inexistant");
        }

    }

    //samir
    @Override
    public Theatre rechercherPiece(String titre, String metteurEnScene) {
        for (Theatre theatre : lesPieces.values()){
            if (theatre.getTitre().equals(titre) && theatre.getNomMetteurEnScene().equals(metteurEnScene)){
                return theatre;
            }
        }
        return null;
    }

    //samir
    @Override
    public void ajouterPiece(String titre, String metteurEnScene, int nbEntractes) {
        if (rechercherPiece(titre, metteurEnScene) == null){
            Theatre theatre = new Theatre(titre, metteurEnScene, nbEntractes);
            lesPieces.put(theatre.getNumero(), theatre);
        }
        else {
            throw new IllegalArgumentException("la piece existe déjà");
        }

    }

    //samir
    @Override
    public void ajouterSeanceFilm(int idFilm, int jour, Horaire debut, int idSalle) {
        if (!existeFilm(idFilm)){
            throw new IllegalArgumentException("film inexistant");
        } else if (!existeSalleFilm(idSalle)){
            throw new IllegalArgumentException("salle inexistante");
        } else {
            Creneau creneau = new Creneau(jour,debut,
                    new Horaire(debut.getHeures() + dureeFilm(idFilm) / 60,
                            debut.getMinutes() + dureeFilm(idFilm) % 60));
            if (!lesSalles.get(idSalle).estDisponible(creneau)) {
                throw new IllegalStateException("Creneau indisponible pour cette salle");
            } else {
                SeanceFilm seancefilm = new SeanceFilm(creneau, lesSalles.get(idSalle));
                lesSalles.get(idSalle).ajouterCreneau(creneau);
                lesFilms.get(idFilm).getSeances().add(seancefilm);
            }
        }
    }

    //samir
    @Override
    public boolean existeSeanceCeJour(int idPiece, int jour) {
        if (existePiece(idPiece)){
            return !lesPieces.get(idPiece).rechercherSeances(jour).isEmpty();
        }
        else {
            throw new IllegalArgumentException("pièce inexistante");
        }
    }

    //samir
    @Override
    public void ajouterSeanceTheatre(int idPiece, int jour, Horaire debut, int idSalle) {
        if (!existePiece(idPiece)){
            throw new IllegalArgumentException("piece inexistante");
        } else if (!existeSalleTheatre(idSalle)) {
            throw new IllegalArgumentException("salle inexistante");
        } else {
            Creneau creneau = new Creneau(jour,debut,new Horaire(debut.getHeures()+3,
                    debut.getMinutes()));
            if (existeSeanceCeJour(idPiece, jour)) {
                throw new IllegalStateException("Un creneau existe déjà à ce jour pour cette salle");
            } else {
                    SeanceTheatre seancetheatre = new SeanceTheatre(creneau, lesSallesTheatre.get(idSalle));
                    lesSallesTheatre.get(idSalle).ajouterCreneau(creneau);
                    lesPieces.get(idPiece).getSeances().add(seancetheatre);
            }
        }
    }

    //samir
    @Override
    public double chiffreAffaires(int numSpectacle) {
        if (estUnFilm(numSpectacle)){
            return lesFilms.get(numSpectacle).chiffreAffaire();
        }
        else if (estUnePiece(numSpectacle)){
            return lesPieces.get(numSpectacle).chiffreAffaire();
        }
        else {
            throw new IllegalArgumentException("spectacle inexistant");
        }
    }

    //samir
    @Override
    public double getTauxRemplissage(int numSpectacle) {
        if (estUnFilm(numSpectacle)){
            return lesFilms.get(numSpectacle).tauxMoyenRemplissage();
        }
        else if (estUnePiece(numSpectacle)){
            return lesPieces.get(numSpectacle).tauxMoyenRemplissage();
        }
        else {
            throw new IllegalArgumentException("spectacle inexistant");
        }
    }

    //remy
    @Override
    public void vendrePlaceFilmTN(int idFilm, int jour, Horaire debut, int nbPlacesTN) {
        if(!existeFilm(idFilm)) {
            throw new IllegalArgumentException("Film inexistant");
        }

        Film film = lesFilms.get(idFilm);
        if (film.rechercherSeance(jour, debut) == null) {
            throw new IllegalArgumentException("seance inexistante");
        }

        film.rechercherSeance(jour, debut).vendrePlacesTN(nbPlacesTN);
    }

    //remy
    @Override
    public void vendrePlaceFilmTR(int idFilm, int jour, Horaire debut, int nbPlacesTR) {
        if(!existeFilm(idFilm)) {
            throw new IllegalArgumentException("Film inexistant");
        }

        Film film = lesFilms.get(idFilm);
        if (film.rechercherSeance(jour, debut) == null) {
            throw new IllegalArgumentException("seance inexistante");
        }

        ((SeanceFilm)film.rechercherSeance(jour, debut)).vendrePlacesTR(nbPlacesTR);
    }

    //remy
    @Override
    public void vendrePlacePieceTN(int idPiece, int jour, int nbPlacesTN) {
        if(!existePiece(idPiece)) {
            throw new IllegalArgumentException("Film inexistant");
        }

        Theatre theatre = lesPieces.get(idPiece);
        Collection<Seance> seances = theatre.rechercherSeances(jour);
        if (seances.isEmpty()) {
            throw new IllegalArgumentException("seance inexistante");
        }
        //sinon
        for (Seance seance : seances) {
            seance.vendrePlacesTN(nbPlacesTN);
        }
    }

    //remy
    @Override
    public void vendrePlaceFauteuilPiece(int idPiece, int jour, int nbFauteuils) {
        if(!existePiece(idPiece)) {
            throw new IllegalArgumentException("Pièce inexistante");
        }

        Theatre theatre = lesPieces.get(idPiece);
        Collection<Seance> seances = theatre.rechercherSeances(jour);
        if (seances.isEmpty()) {
            throw new IllegalArgumentException("seance inexistante");
        }
        //sinon
        for (Seance seance : seances) {
            ((SeanceTheatre)seance).vendrePlacesFauteuil(nbFauteuils);
        }

    }

    //remy
    @Override
    public String lesFilms() {
        if(lesFilms.isEmpty()){
            throw new IllegalArgumentException("Aucun film");
        }
        String s= "";
        for(Film film : lesFilms.values()){
            s+=film+"\n\n";
        }
        return s;
    }

    //remy
    @Override
    public String lesPieces() {
        if(lesPieces.isEmpty()){
            throw new IllegalArgumentException("Aucune pièce");
        }
        String s = "";
        for(Theatre theatre : lesPieces.values()){
            s+=theatre+"\n\n";
        }
        return s;
    }

    //remy
    @Override
    public String lesSallesFilm() {
        if(lesSalles.isEmpty()){
            throw new IllegalArgumentException("Aucune salle");
        }
        String s = "";
        for(Salle salle : lesSalles.values()){
            s+=salle+"\n\n";
        }
        return s;
    }

    //remy
    @Override
    public String lesSallesTheatre() {
        if(lesSallesTheatre.isEmpty()){
            throw new IllegalArgumentException("Aucune salle de theatre");
        }
        String s = "";
        for(SalleTheatre salleTheatre : lesSallesTheatre.values()){
            s+=salleTheatre+"\n\n";
        }
        return s;
    }

    //remy
    @Override
    public String lesSeancesTheatre(int idPiece) {
        if(existePiece(idPiece)) {
            Collection<Seance> seance = lesPieces.get(idPiece).getSeances();
            if(seance.isEmpty()){
                throw new IllegalArgumentException("Aucune séance");
            }
            String lesSeances = "";
            for(Seance s : seance){
                lesSeances+= ((SeanceTheatre) s).toString()+"\n\n";
            }
            return lesSeances;
        }
        throw new IllegalArgumentException("Piece inexistante");
    }

    //remy
    @Override
    public String lesSeancesFilm(int idFilm) {
        if(existeFilm(idFilm)) {
            Collection<Seance> seance = lesFilms.get(idFilm).getSeances();
            if(seance.isEmpty()){
                throw new IllegalArgumentException("Aucune séance");
            }
            String lesSeances = "";
            for(Seance s : seance){
                lesSeances+=((SeanceFilm) s).toString()+"\n\n";
            }
            return lesSeances;
        }
        throw new IllegalArgumentException("Film inexistant");
    }

    //remy
    @Override
    public int getNbPlacesDispo(int numSpectacle, int jour, int heures, int minutes) {
        if(!existeFilm(numSpectacle)) {
            throw new IllegalArgumentException("Spectacle inexistant");
        }

        Film film = lesFilms.get(numSpectacle);
        Seance seance = film.rechercherSeance(jour, new Horaire(heures,minutes));
        if (seance == null) {
            throw new IllegalArgumentException ("Séance inexistante pour ce spectacle");
        }

        return seance.getNbPlaceDispo();
    }

    //soumia
    @Override
    public boolean existeFilm(int idFilm) {
        return lesFilms.containsKey(idFilm);
    }

    //soumia
    @Override
    public boolean existePiece(int idPiece) {
        return lesPieces.containsKey(idPiece);

    }

    //soumia
    @Override
    public boolean existeSeanceFilm(int idFilm, int jour, int heures, int minutes) {
        if(existeFilm(idFilm)) {
            return lesFilms.get(idFilm).rechercherSeance(jour, new Horaire(heures, minutes)) != null;
        }
        else{
            throw new IllegalArgumentException("film inexistant");
        }
    }

    //soumia
    @Override
    public boolean existeSalleFilm(int idSalle) {
        return lesSalles.containsKey(idSalle);
    }

    //soumia
    @Override
    public boolean existeSalleTheatre(int idSalle) {
        return lesSallesTheatre.containsKey(idSalle);
    }

    //samir
    @Override
    public int dureeFilm(int idFilm) {
        if (existeFilm(idFilm)){
            return lesFilms.get(idFilm).getDuree();
        }
        else {
            throw new IllegalArgumentException("film inexistant");
        }
    }

    //samir
    @Override
    public boolean salleStandardDisponible(int jour, Horaire debut, int duree, int idSalle) {
        return existeSalleFilm(idSalle) && lesSalles.get(idSalle).estDisponible(new Creneau(jour,debut,new Horaire(debut.getHeures()+duree/60,
                    debut.getMinutes()%60)));
    }

    //samir
    @Override
    public void reinitialiserProgrammation() {
        for (Salle salle : lesSalles.values()){
            salle.supprimerCreneauOccupees();
        }
        for (SalleTheatre salletheatre : lesSallesTheatre.values()){
            salletheatre.supprimerCreneauOccupees();
        }
        lesFilms.clear();
        lesPieces.clear();
    }

    //soumia
    @Override
    public int getNbFauteuilsDispo(int idPiece, int jour) {
        if(!existePiece(idPiece)){
            throw new IllegalArgumentException("Piece inexistante");
        }

        Theatre theatre = lesPieces.get(idPiece);
        if (!theatre.rechercherSeances(jour).isEmpty()) {
            Collection<Seance> seance = lesPieces.get(idPiece).rechercherSeances(jour);
            for (Seance s : seance) {
                return ((SeanceTheatre) s).getNbFauteuilsDispo();
            }
        }
        throw new IllegalArgumentException("Pièce inexistante");
    }

    //remy
    @Override
    public int getNbPlacesDispo(int idPiece, int jour) {
        if(!existePiece(idPiece)) {
            throw new IllegalArgumentException("Pièce inexistante");
        }

        Collection<Seance> seances = lesPieces.get(idPiece).rechercherSeances(jour);
        if(!seances.isEmpty()){
            for(Seance s : seances){
                return ((SeanceTheatre)s).getNbPlaceDispo();
            }
        }
        throw new IllegalArgumentException("Aucune séance ce jour");
    }

    //samir
    @Override
    public boolean estUnFilm(int numSpectacle) {
        return lesFilms.containsKey(numSpectacle);
    }

    //samir
    @Override
    public boolean estUnePiece(int numSpectacle) {
        return lesPieces.containsKey(numSpectacle);
    }

    //samir
    @Override
    public Spectacle getSpectacle(int numSpectacle) {
        if (estUnFilm(numSpectacle)){
            return lesFilms.get(numSpectacle);
        }
        else if(estUnePiece(numSpectacle)){
            return lesPieces.get(numSpectacle);
        }
        return null;
    }
}
