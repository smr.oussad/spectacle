package projetPOO;

import java.util.*;

public abstract class Spectacle {

    private int numero;

    private String titre;

    private Collection<String> interpretes;

    protected Collection<Seance> seances;

    protected Spectacle(int numero, String titre) {
        this.numero = numero;
        this.titre = titre;
        this.seances= new HashSet<>();
        this.interpretes = new ArrayList<>();
    }

    /**
     * parcours la liste de seance qu'on récupère à partir de la méthode rechercherSeances pour permettre de parcourir que les seances qui existe à ce jour précis.
     * puis on cherche la séance qui correspond bien au jour et à l'horaire de début.
     * @param jour compris entre 1 et 7
     * @param debut heure entre 0 et 23 et minutes entre 0 et 59.
     * @return la seance si la seance existe sinon null.
     */
    public Seance rechercherSeance(final int jour, final Horaire debut) {
        for (Seance seance : rechercherSeances(jour)){
            if (debut.equals(seance.getdebut())){
                return seance;
            }
        }
        return null;
    }

    /**
     * methode de type abstract car elle n'avait pas le même comportement pour Film et Theatre.
     * @param jour
     * @return
     */
    public abstract Collection<Seance> rechercherSeances(int jour);

    /**
     * calcul du tauxMoyenRemplissage à partir du tauxRemplissage.
     * @return la valeur.
     */
    public double tauxMoyenRemplissage() {
        double taux = 0;
        for (Seance seance : seances){
            taux += seance.tauxRemplissage();
        }
        return taux/seances.size();
    }

    /**
     * methode de type abstract car elle n'avait pas le même comportement pour Film et Theatre.
     * @return
     */
    public abstract double chiffreAffaire ();

    public int getNumero() {
        return numero;
    }

    public String getTitre() {
        return titre;
    }

    public Collection<String> getInterpretes() {
        return interpretes;
    }

    public Collection<Seance> getSeances() {
        return seances;
    }

    @Override
    public String toString() {
        String s = "Spectacle n°"+numero+"\n"+"Titre : "+titre+"\n"+"interprete :";
        String ajout="";
        for (String interprete : interpretes){
            ajout += interprete;
        }
        return s + ajout;
    }
}
