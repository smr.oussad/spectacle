package projetPOO.gestionSpectacle;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import projetPOO.Film;
import projetPOO.Horaire;
import projetPOO.Theatre;

public class GestionProgrammationSemaineTest {

    private IProgrammationSemaine programmationSemaine;

    private int idSalleFilm;

    private int idSalleTheatre;

    @Before
    public void setUp() {
        programmationSemaine = new GestionProgrammationSemaine();
        programmationSemaine.ajouterFilm("Star Wars", "Georges Lucas", 143);
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        final String lesSallesFilms = programmationSemaine.lesSallesFilm();
        idSalleFilm = Integer.parseInt(lesSallesFilms.substring((lesSallesFilms.indexOf("°") + 1), lesSallesFilms.indexOf("\n")));
        programmationSemaine.ajouterSeanceFilm(film.getNumero(), 1, new Horaire(10,0), idSalleFilm);
        programmationSemaine.ajouterPiece("Roméo et Juliette", "Shakespeare", 1);
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        final String lesSallesTheatres = programmationSemaine.lesSallesTheatre();
        idSalleTheatre = Integer.parseInt(lesSallesTheatres.substring((lesSallesTheatres.indexOf("°") + 1), lesSallesTheatres.indexOf("\n")));
        programmationSemaine.ajouterSeanceTheatre(theatre.getNumero(), 1, new Horaire(14, 0), idSalleTheatre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterFilmExistantTest() {
        programmationSemaine.ajouterFilm("Star Wars", "Georges Lucas", 143);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterPieceExistanteTest() {
        programmationSemaine.ajouterPiece("Roméo et Juliette", "Shakespeare", 1);
    }

    @Test
    public void rechercherFilmExistantTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertEquals("Georges Lucas", film.getNomRealisateur());
        Assert.assertEquals("Star Wars", film.getTitre());
        Assert.assertEquals(143, film.getDuree());
    }

    @Test
    public void rechercherFilmNonExistantTest() {
        Assert.assertNull(programmationSemaine.rechercherFilm("Avengers", "Joss Whedon"));
    }

    @Test
    public void rechercherPieceExistanteTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        Assert.assertEquals("Shakespeare", theatre.getNomMetteurEnScene());
        Assert.assertEquals("Roméo et Juliette", theatre.getTitre());
        Assert.assertEquals(1, theatre.getNbEntractes());
    }

    @Test
    public void rechercherPieceNonExistanteTest() {
        Assert.assertNull(programmationSemaine.rechercherPiece("Le médecin malgré lui", "Molière"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void existeSeanceCeJourPieceInexistanteTest() {
        programmationSemaine.existeSeanceCeJour(1, 1);
    }

    @Test
    public void existeSeanceCeJourTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        Assert.assertTrue(programmationSemaine.existeSeanceCeJour(theatre.getNumero(), 1));
    }

    @Test
    public void ajouterInterpreteFilmExistantTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.ajouterInterprete(film.getNumero(), "Sam");

        Assert.assertTrue(film.getInterpretes().contains("Sam"));
    }

    @Test
    public void ajouterInterpreteTheatreExistantTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.ajouterInterprete(theatre.getNumero(), "Sam");

        Assert.assertTrue(theatre.getInterpretes().contains("Sam"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterInterpreteSpectacleInexistantTest() {
        programmationSemaine.ajouterInterprete(1, "Sam");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterSeanceFilmInexsistantTest() {
        programmationSemaine.ajouterSeanceFilm(1, 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterSeanceFilmSalleInexistanteTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.ajouterSeanceFilm(film.getNumero(), 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalStateException.class)
    public void ajouterSeanceFilmCreneauIndisponibleTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.ajouterSeanceFilm(film.getNumero(), 1, new Horaire(11, 0), idSalleFilm);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterSeanceTheatreInexsistantTest() {
        programmationSemaine.ajouterSeanceTheatre(1, 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ajouterSeanceTheatreSalleInexistanteTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.ajouterSeanceTheatre(theatre.getNumero(), 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalStateException.class)
    public void ajouterSeanceTheatreCreneauIndisponibleTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.ajouterSeanceTheatre(theatre.getNumero(), 1, new Horaire(15, 0), idSalleTheatre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void chiffreAffaireSpectacleInexistantTest() {
        programmationSemaine.chiffreAffaires(1);
    }

    @Test
    public void chiffreAffaireFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertThat(programmationSemaine.chiffreAffaires(film.getNumero()), is(0.0));
    }

    @Test
    public void chiffreAffaireTheatreTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        Assert.assertThat(programmationSemaine.chiffreAffaires(theatre.getNumero()), is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getTauxRemplissageSpectacleInexistantTest() {
        programmationSemaine.getTauxRemplissage(1);
    }

    @Test
    public void getTauxRemplissageFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertThat(programmationSemaine.getTauxRemplissage(film.getNumero()), is(0.0));
    }

    @Test
    public void getTauxRemplissageTheatreTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        Assert.assertThat(programmationSemaine.getTauxRemplissage(theatre.getNumero()), is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFilmTNFilmInexistantTest() {
        programmationSemaine.vendrePlaceFilmTN(1, 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFilmTNCreneauIndispoTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.vendrePlaceFilmTN(film.getNumero(), 2, new Horaire(10, 0), 1);
    }

    @Test
    public void vendrePlaceFilmTNTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.vendrePlaceFilmTN(film.getNumero(), 1, new Horaire(10, 0), 5);
        Assert.assertEquals(programmationSemaine.getNbPlacesDispo(film.getNumero(), 1, 10, 0), 40);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFilmTRFilmInexistantTest() {
        programmationSemaine.vendrePlaceFilmTR(1, 1, new Horaire(10, 0), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFilmTRCreneauIndispoTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.vendrePlaceFilmTR(film.getNumero(), 2, new Horaire(10, 0), 1);
    }

    @Test
    public void vendrePlaceFilmTRTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.vendrePlaceFilmTR(film.getNumero(), 1, new Horaire(10, 0), 5);
        Assert.assertEquals(programmationSemaine.getNbPlacesDispo(film.getNumero(), 1, 10, 0), 40);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceTheatreTNPieceInexistanteTest() {
        programmationSemaine.vendrePlacePieceTN(1, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceTheatreTNCreneauIndispoTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.vendrePlacePieceTN(theatre.getNumero(), 2, 1);
    }

    @Test
    public void vendrePlaceTheatreTNTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.vendrePlacePieceTN(theatre.getNumero(), 1, 5);
        Assert.assertEquals(programmationSemaine.getNbPlacesDispo(theatre.getNumero(), 1), 40);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFauteuilPieceInexistanteTest() {
        programmationSemaine.vendrePlaceFauteuilPiece(1, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vendrePlaceFauteuilCreneauIndispoTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.vendrePlaceFauteuilPiece(theatre.getNumero(), 2, 1);
    }

    @Test
    public void vendrePlaceFauteuilTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.vendrePlaceFauteuilPiece(theatre.getNumero(), 1, 5);
        Assert.assertEquals(programmationSemaine.getNbFauteuilsDispo(theatre.getNumero(), 1), 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dureeFilmInexistantTest() {
        programmationSemaine.dureeFilm(1);
    }

    @Test
    public void dureeFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertEquals(143, programmationSemaine.dureeFilm(film.getNumero()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void reinitProgrammation() {
        programmationSemaine.reinitialiserProgrammation();
        programmationSemaine.lesFilms();
    }

    @Test(expected = IllegalArgumentException.class)
    public void lesSeancesTheatreInexistantesTest() {
        programmationSemaine.lesSeancesTheatre(1);
    }

    @Test
    public void lesSeancesTheatreTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        programmationSemaine.lesSeancesTheatre(theatre.getNumero());
    }

    @Test(expected = IllegalArgumentException.class)
    public void lesSeancesFilmInexistantesTest() {
        programmationSemaine.lesSeancesFilm(1);
    }

    @Test
    public void lesSeancesFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        programmationSemaine.lesSeancesFilm(film.getNumero());
    }

    @Test
    public void getSpectacleTheatreTest() {
        final Theatre theatre = programmationSemaine.rechercherPiece("Roméo et Juliette", "Shakespeare");
        Assert.assertEquals(theatre, programmationSemaine.getSpectacle(theatre.getNumero()));
    }

    @Test
    public void getSpectacleFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertEquals(film, programmationSemaine.getSpectacle(film.getNumero()));
    }

    @Test
    public void getSpectacleInexistantTest() {
        Assert.assertNull(programmationSemaine.getSpectacle(1));
    }

    @Test
    public void lesFilmsTest() {
        programmationSemaine.lesFilms();
    }

    @Test
    public void lesPiecesTest() {
        programmationSemaine.lesPieces();
    }

    @Test
    public void salleStandardDisponible() {
        Assert.assertFalse(programmationSemaine.salleStandardDisponible(1, new Horaire(9, 0), 120, idSalleFilm));
        Assert.assertTrue(programmationSemaine.salleStandardDisponible(1, new Horaire(9, 0), 60, idSalleFilm));
        Assert.assertFalse(programmationSemaine.salleStandardDisponible(1, new Horaire(9, 0), 60, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void existeSeanceFilmInexistantTest() {
        programmationSemaine.existeSeanceFilm(1, 1, 10, 0);
    }

    @Test
    public void existeSeanceFilmTest() {
        final Film film = programmationSemaine.rechercherFilm("Star Wars", "Georges Lucas");
        Assert.assertTrue(programmationSemaine.existeSeanceFilm(film.getNumero(), 1, 10, 0));
        Assert.assertFalse(programmationSemaine.existeSeanceFilm(film.getNumero(), 1, 11, 0));
    }
}
