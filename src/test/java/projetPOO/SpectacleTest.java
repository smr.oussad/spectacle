package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SpectacleTest {

	private Spectacle spectacle;

	@Before
	public void setUp() {
		spectacle = new Film("Star Wars", "Georges Lucas", 230);
	}

	@Test
	public void rechercherSeanceMatinExistanteTest() throws Exception {
		final SeanceFilm seanceMatin =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));
		final SeanceFilm seanceAprem =
				new SeanceFilm(new Creneau(2, new Horaire(16, 0), new Horaire(18, 40)),
						new Salle(50, "Ice", 12.5));

		spectacle.seances.add(seanceMatin);
		spectacle.seances.add(seanceAprem);

		Assert.assertEquals(seanceMatin, spectacle.rechercherSeance(1, new Horaire(10, 0)));
	}

	@Test
	public void rechercherSeanceMatinNonExistanteTest() throws Exception {
		Assert.assertNull(spectacle.rechercherSeance(1, new Horaire(10, 0)));
	}

	@Test
	public void tauxRemplissageTest() {
		final SeanceFilm seanceMatin =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));
		final SeanceFilm seanceAprem =
				new SeanceFilm(new Creneau(2, new Horaire(16, 0), new Horaire(18, 40)),
						new Salle(50, "Ice", 12.5));
		spectacle.seances.add(seanceMatin);
		spectacle.seances.add(seanceAprem);

		seanceMatin.vendrePlacesTN(10);
		seanceMatin.vendrePlacesTR(15);
		seanceAprem.vendrePlacesTN(15);
		seanceAprem.vendrePlacesTN(10);

		Assert.assertThat(spectacle.tauxMoyenRemplissage(), is(50.0));
	}
}
