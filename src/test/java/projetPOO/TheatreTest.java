package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TheatreTest {

	private Theatre theatre;

	@Before
	public void setUp() {
		theatre = new Theatre("Le médecin malgré lui", "Molière", 2);
	}

	@Test
	public void ajouterSeanceTheatreInexistanteTest() {
		final SeanceTheatre seance =
				new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 0, 70));
		theatre.ajouterSeanceTheatre(seance);

		final SeanceTheatre seanceMatin =
				new SeanceTheatre(new Creneau(2, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 0, 70));

		Assert.assertTrue(theatre.ajouterSeanceTheatre(seanceMatin));
	}

	@Test
	public void ajouterSeanceTheatreExistanteTest() {
		final SeanceTheatre seance =
				new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 0, 70));
		theatre.ajouterSeanceTheatre(seance);

		final SeanceTheatre seanceMatin =
				new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 0, 70));

		Assert.assertFalse(theatre.ajouterSeanceTheatre(seanceMatin));
	}

	@Test
	public void gettersTheatreTest() {
		Assert.assertEquals("Le médecin malgré lui", theatre.getTitre());
		Assert.assertEquals("Molière", theatre.getNomMetteurEnScene());
		Assert.assertEquals(2, theatre.getNbEntractes());
	}

	@Test
	public void rechercherSeancesTest() throws Exception {
		final SeanceTheatre seance =
				new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 0, 70));

		theatre.ajouterSeanceTheatre(seance);

		Assert.assertTrue(theatre.rechercherSeances(1).contains(seance));
	}

	@Test
	public void chiffreAffaireTest() {
		final SeanceTheatre seance =
				new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new SalleTheatre(50, "Einstein", 12.5, 50, 70));

		theatre.ajouterSeanceTheatre(seance);

		seance.vendrePlacesTN(25);
		seance.vendrePlacesFauteuil(25);

		Assert.assertThat(theatre.chiffreAffaire(), is(2062.5));
	}
}
