package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SeanceTheatreTest {

	private SeanceTheatre seance;

	@Before
	public void setUp() {
		seance = new SeanceTheatre(new Creneau(1, new Horaire(10, 0), new Horaire(11, 0)),
				new SalleTheatre(100, "Molière", 12, 50, 25));
	}

	@Test
	public void vendrePlaceTN_OK() {
		seance.vendrePlacesTN(50);
		Assert.assertEquals(50, seance.getnbplaceVendueTarifNormal());
		Assert.assertEquals(0, seance.getNbFauteuilsVendues());
	}

	@Test(expected = IllegalArgumentException.class)
	public void vendrePlaceTN_KO() {
		seance.vendrePlacesTN(50);
		seance.vendrePlacesTN(51);
	}

	@Test
	public void vendrePlacesFauteuil_OK() {
		seance.vendrePlacesFauteuil(50);
		Assert.assertEquals(0, seance.getnbplaceVendueTarifNormal());
		Assert.assertEquals(50, seance.getNbFauteuilsVendues());
	}

	@Test(expected = IllegalArgumentException.class)
	public void vendrePlacesFauteuil_KO() {
		seance.vendrePlacesFauteuil(50);
		seance.vendrePlacesFauteuil(1);
	}

	@Test
	public void tauxReplissageTest() {
		Assert.assertThat(seance.tauxRemplissage(), is(0.0));
		seance.vendrePlacesTN(50);
		seance.vendrePlacesFauteuil(25);
		Assert.assertThat(seance.tauxRemplissage(), is(50.0));
		seance.vendrePlacesTN(50);
		seance.vendrePlacesFauteuil(25);
		Assert.assertThat(seance.tauxRemplissage(), is(100.0));
	}
}
