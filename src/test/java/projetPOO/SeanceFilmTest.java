package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SeanceFilmTest {

	private SeanceFilm seance;

	@Before
	public void setUp() {
		seance = new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(11, 0)),
				new Salle(50, "Ice", 12.5));
	}

	@Test
	public void vendrePlaceTN_TR_OK() {
		seance.vendrePlacesTN(49);
		seance.vendrePlacesTR(1);
		Assert.assertEquals(49, seance.getnbplaceVendueTarifNormal());
		Assert.assertEquals(1, seance.getNbPlacesVenduesTarifReduit());
	}

	@Test(expected = IllegalArgumentException.class)
	public void vendrePlaceTN_TR_KO() {
		seance.vendrePlacesTR(50);
		seance.vendrePlacesTN(1);
	}

	@Test
	public void tauxReplissageTest() {
		Assert.assertThat(seance.tauxRemplissage(), is(0.0));
		seance.vendrePlacesTN(25);
		Assert.assertThat(seance.tauxRemplissage(), is(50.0));
		seance.vendrePlacesTR(25);
		Assert.assertThat(seance.tauxRemplissage(), is(100.0));
	}
}
