package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SalleTest {

	private Salle salle;

	private Creneau creneau;

	@Before
	public void setUp() {
		creneau = new Creneau(1, new Horaire(10, 0), new Horaire(12, 0));
		salle = new Salle(50, "Ice", 12.5);
	}

	@Test
	public void salleEquals() {
		Assert.assertEquals(salle, salle);
	}

	@Test
	public void salleGetters() {
		Assert.assertEquals(50, salle.getNbplace());
		Assert.assertEquals("Ice", salle.getNomSalle());
		Assert.assertThat(salle.getTarifplace(), is(12.5));
		Assert.assertThat(salle.getTarifplaceReduit(), is(7.5));
	}

	@Test
	public void salleNotEquals() {
		final Salle salle = new Salle(100, "Salle 1", 8.5);
		Assert.assertNotEquals(this.salle, salle);
	}

	@Test
	public void ajouterCreneauDispoTest() {
		final Creneau creneauDispo = new Creneau(1, new Horaire(9, 0), new Horaire(10, 0));

		Assert.assertTrue(salle.ajouterCreneau(creneau));
		Assert.assertTrue(salle.ajouterCreneau(creneauDispo));
	}

	@Test
	public void ajouterCreneauPasDispoTest() {
		final Creneau creneauPasDispo = new Creneau(1, new Horaire(9, 30), new Horaire(11, 0));

		Assert.assertTrue(salle.ajouterCreneau(creneau));
		Assert.assertFalse(salle.ajouterCreneau(creneauPasDispo));
	}

	@Test
	public void salleDisponibleTest() {
		final Creneau creneauDispo = new Creneau(1, new Horaire(9, 0), new Horaire(10, 0));

		salle.ajouterCreneau(creneau);

		Assert.assertTrue(salle.estDisponible(creneauDispo));
	}

	@Test
	public void salleNonDisponibleTest() {
		final Creneau creneauPasDispo = new Creneau(1, new Horaire(9, 30), new Horaire(11, 0));

		salle.ajouterCreneau(creneau);

		Assert.assertFalse(salle.estDisponible(creneauPasDispo));
	}

	@Test
	public void salleCreneauACeJourTest() {
		salle.ajouterCreneau(creneau);

		Assert.assertFalse(salle.pasDeCreneauCeJour(1));
	}

	@Test
	public void sallePasDeCreneauACeJourTest() {
		salle.ajouterCreneau(creneau);

		Assert.assertTrue(salle.pasDeCreneauCeJour(2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void sallePasDeCreneauACeJourImpossibleTest() {
		salle.pasDeCreneauCeJour(8);
	}
}
