package projetPOO;

import static org.hamcrest.core.Is.is;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FilmTest {

	private Film film;

	@Before
	public void setUp() {
		film = new Film("star wars", "Georges Lucas", 143);
	}

	@Test
	public void gettersFilmTest() {
		Assert.assertEquals("star wars", film.getTitre());
		Assert.assertEquals("Georges Lucas", film.getNomRealisateur());
		Assert.assertEquals(143, film.getDuree());
	}

	@Test
	public void ajouterSeanceFilmInexistanteTest() {
		final SeanceFilm seance =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));
		film.ajouterSeanceFilm(seance);

		final SeanceFilm seanceMatin =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Salle 1", 12.5));

		Assert.assertTrue(film.ajouterSeanceFilm(seanceMatin));
	}

	@Test
	public void ajouterSeanceFilmExistanteTest() {
		final SeanceFilm seance =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));
		film.ajouterSeanceFilm(seance);

		final boolean result = film.ajouterSeanceFilm(seance);

		Assert.assertFalse(result);
	}

	@Test
	public void rechercherSeancesTest() throws Exception {
		final SeanceFilm seanceMatin =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));
		final SeanceFilm seanceFinMatin =
				new SeanceFilm(new Creneau(1, new Horaire(11, 0), new Horaire(13, 40)),
						new Salle(50, "Salle 1", 12.5));
		final SeanceFilm seanceAprem =
				new SeanceFilm(new Creneau(2, new Horaire(16, 0), new Horaire(18, 40)),
						new Salle(50, "Ice" , 12.5));

		film.ajouterSeanceFilm(seanceMatin);
		film.ajouterSeanceFilm(seanceFinMatin);
		film.ajouterSeanceFilm(seanceAprem);

		Assert.assertTrue(film.rechercherSeances(1).contains(seanceMatin));
		Assert.assertTrue(film.rechercherSeances(1).contains(seanceFinMatin));
		Assert.assertTrue(film.rechercherSeances(2).contains(seanceAprem));
	}

	@Test
	public void chiffreAffaireTest() {
		final SeanceFilm seance =
				new SeanceFilm(new Creneau(1, new Horaire(10, 0), new Horaire(12, 40)),
						new Salle(50, "Ice", 12.5));

		film.ajouterSeanceFilm(seance);

		seance.vendrePlacesTN(25);
		seance.vendrePlacesTR(25);

		Assert.assertThat(film.chiffreAffaire(), is(500.0));
	}
}
