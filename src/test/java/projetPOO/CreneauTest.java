package projetPOO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CreneauTest {

    private Creneau creneau;

    @Before
    public void setUp() {
        creneau = new Creneau(1, new Horaire(10, 0), new Horaire(11, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void creationCreneauJour_KO() {
        creneau = new Creneau(8, new Horaire(0, 0), new Horaire(0, 0));
    }

    @Test
    public void creationCreneau_OK() {
        Assert.assertEquals("Le jour attendu lors de la création d'un créneau est incorrect", 1, creneau.getJours());
        Assert.assertEquals(10, creneau.getDebut().getHeures());
        Assert.assertEquals(0, creneau.getDebut().getMinutes());
        Assert.assertEquals(11, creneau.getFin().getHeures());
        Assert.assertEquals(0, creneau.getFin().getMinutes());
    }

    @Test
    public void CreneauDispo() {
        Assert.assertTrue(creneau.estDispo(1, new Horaire(9, 0), new Horaire(10, 0)));
    }

    @Test
    public void CreneauPasDispo() {
        Assert.assertFalse(creneau.estDispo(1, new Horaire(9, 0), new Horaire(11, 0)));
    }

    @Test
    public void CreneauEquals() {
        final Creneau creneauIdentique = new Creneau(1, new Horaire(10, 0), new Horaire(11, 0));

        Assert.assertEquals(creneau, creneauIdentique);
    }

    @Test
    public void CreneauNotEquals() {
        final Creneau creneauNonIdentique = new Creneau(1, new Horaire(0, 0), new Horaire(0, 0));

        Assert.assertNotEquals(creneau, creneauNonIdentique);
    }

}
