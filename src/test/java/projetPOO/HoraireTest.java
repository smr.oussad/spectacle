package projetPOO;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HoraireTest {

    private Horaire horaire;

    @Before
    public void setUp() {
        horaire = new Horaire(10, 30);
    }

    @Test
    public void horaireGetters() {
        Assert.assertEquals(10, horaire.getHeures());
        Assert.assertEquals(30, horaire.getMinutes());
    }

    @Test
    public void horaireEquals() {
        Horaire horaireIdentique = new Horaire(10, 30);

        Assert.assertEquals(horaire, horaireIdentique);
    }

    @Test
    public void horaireNotEquals() {
        Horaire horaireNonIdentique = new Horaire(10, 31);

        Assert.assertNotEquals(horaire, horaireNonIdentique);
    }

    @Test
    public void compareAvecHorairePassee() {
        Horaire horairePassee = new Horaire(10, 29);
        Assert.assertEquals(-1, horaire.compareTo(horairePassee));
    }

    @Test
    public void compareAvecHoraireAvenir() {
        Horaire horaireAvenir = new Horaire(10, 31);
        Assert.assertEquals(1, horaire.compareTo(horaireAvenir));
    }

    @Test
    public void compareAvecHoraireIdentique() {
        Horaire horaireIdentique = new Horaire(10, 30);
        Assert.assertEquals(0, horaire.compareTo(horaireIdentique));
    }

}
