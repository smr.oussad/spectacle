# Spectacle

Projet de L2 Informatique à l'université d'Orléans pour la validation du module Programmation Orientée Objet.

## Architecture technique du projet

Voici le diagramme UML caractérisant la conception de ce projet:
![alt text](Spectacle.png "diagramme spectacle")

## Integration continue

Un pipeline est mis en place sur ce projet à l'aide du fichier **.gitlab-ci.yml** pour jouer les tests unitaires mise en place.
Ainsi, cela nous permet de garantir la stabilité de notre branche **master**.

Pour plus d'information, voir la documentation [gitlab](https://docs.gitlab.com/ee/ci/)

![pipeline](https://gitlab.com/smr.oussad/spectacle/badges/master/pipeline.svg)
![coverage](https://gitlab.com/smr.oussad/spectacle/badges/master/coverage.svg?job=test)

## Lancer le projet


```xml
mvn clean install
```

Cette commande génerera un **JAR** exécutable dans le répertoire **target/**
Il vous suffira alors d'exécuter la commande suivante:

```xml
java -jar nomFicher.jar
```

## Documentation de l'API via javadoc
